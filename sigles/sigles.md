# Sigles

- REPL read–eval–print loop: On tape du code et il est exécuté dans la foulée.
- ADR Architecture Decision Records: suivre l'évolution du produit en notant les décisions prises et pourquoi elles ont été prises ainsi.
- RFC Request For Comments: les standards d'internets sont basés dessus.
- DDD Domain Driver Design
- BDD Behaviour Driver Development: on écrit les spécifications et on en déduit les tests d'acceptance
- ATDD Acceptance Test Driven Development: on écrit les tests d'acceptance et on en déduits les spécifications